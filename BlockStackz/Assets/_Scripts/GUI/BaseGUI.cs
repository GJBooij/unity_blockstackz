﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseGUI : MonoBehaviour
{
    private void Awake()
    {
#if !UNITY_EDITOR
        CanvasScaler canvasScaler = GetComponent<CanvasScaler>();

        if (canvasScaler != null)
        {
            if (Utils.DeviceIsTablet())
            {
                canvasScaler.referenceResolution = new Vector2(1024, 680);
            }
            else
            {
                canvasScaler.referenceResolution = new Vector2(800, 600);
            }
        }
#endif
    }
}
