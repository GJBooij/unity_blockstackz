﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainSceneGUI : BaseGUI
{
    [Header("Onboarding")]
    public GameObject onboardingMove, onboardingForce, onboardingSave;

    [Header("Game")]
    public GameObject levelHolder, highestLevelHolder;
    public GameObject comboUI;

    [Header("Buttons & Popups")]
    public GameObject popupStart;
    public GameObject popupGameOver;
    public GameObject buttonSettings, popupSettings;
    public GameObject buttonPause, popupPause;
    public GameObject buttonShare;
    public GameObject leaderboardButton, popupLeaderboard;
    public Button buttonSaveGame;
    public GameObject buttonReloadSave;

    // Start is called before the first frame update
    private void Start()
    {
        //Start listening to events
        GameController.instance.GameOverEvent += On_GameOverEvent;
        GameController.instance.StartGameEvent += On_StartGameEvent;
        GameController.instance.RestartGameEvent += On_RestartGameEvent;

        //Hide or show panels
        popupGameOver.SetActive(false);
        levelHolder.SetActive(false);
        buttonSettings.SetActive(false);
        buttonPause.SetActive(false);
        popupSettings.SetActive(false);
        popupPause.SetActive(false);
        popupLeaderboard.SetActive(false);
        comboUI.SetActive(false);
        buttonReloadSave.SetActive(false);
        popupStart.SetActive(true);

        if (ScoreController.instance.GetHighestLevel() > 2)
        {
            highestLevelHolder.SetActive(true);
            buttonShare.SetActive(true);
            leaderboardButton.SetActive(true);

            //Update the leaderboard
            LeaderboardController.instance.AddScoreToLeaderboard((int)Mathf.Pow(2, ScoreController.instance.GetHighestLevel()), ScoreController.instance.GetHighscore());
        }
        else
        {
            highestLevelHolder.SetActive(false);
            buttonShare.SetActive(false);
            leaderboardButton.SetActive(false);
        }
    }

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    SetOnboarded(false);
        //    SetOnboardedSaveGame(false);
        //}
    }

    public void CompleteOnboarding()
    {
        AfterOnboarding();
        SetOnboarded();
    }

    public void CompleteOnboardingSaveGame()
    {
        AfterOnboarding();
        SetOnboardedSaveGame();
        GameController.instance.PauseGame();
        popupPause.SetActive(true);
    }

    void AfterOnboarding()
    {
        onboardingMove.SetActive(false);
        onboardingForce.SetActive(false);
        onboardingSave.SetActive(false);
        GameController.instance.onboarded = true;

        ShowGameUI();
    }

    public void ShareHighScore()
    {
        //Sound
        SoundController.instance.PlayButtonClick();

        //Analytics
        AnalyticsController.instance.LogEvent("share_score");

        //Share mechanism
        NativeShare share = new NativeShare();
        share.SetTitle("BlockStackz!");
        share.SetText("My BlockStackz stats"
            + "\nHighest block: " + ((long)Mathf.Pow(2, ScoreController.instance.GetHighestLevel())).ToString()
            + "\nHighest score: " + ScoreController.instance.GetHighscore()
#if UNITY_ANDROID
            + "\nCan you beat it? Download the game from the Google Play Store: https://play.google.com/store/apps/details?id=com.zephirostudios.blockstackz");
#else
            + "\nCan you beat it?");
#endif
        share.Share();
    }

    public void OpenSettings()
    {
        popupSettings.SetActive(true);

        //Sound
        SoundController.instance.PlayButtonClick();

        //Analytics
        AnalyticsController.instance.LogEvent("settings_open");
    }

    public void CloseSettings()
    {
        popupSettings.SetActive(false);

        //Sound
        SoundController.instance.PlayButtonClick();

        //Analytics
        AnalyticsController.instance.LogEvent("settings_closed", new Firebase.Analytics.Parameter[] {
                new Firebase.Analytics.Parameter(
                    "background", SettingsController.instance.GetBackground().name),
                new Firebase.Analytics.Parameter(
                    "blocktext", SettingsController.instance.GetBlockText().ToString())
            }
        );
    }

    public void ClickedSaveGame()
    {
        buttonSaveGame.interactable = false;
        SaveGameController.instance.SaveGame();

        //Sound
        SoundController.instance.PlayButtonClick();
    }

    public void ClickedContinue()
    {
        GameController.instance.ContinueGame();
        popupPause.SetActive(false);
        buttonSaveGame.interactable = true;

        //Sound
        SoundController.instance.PlayButtonClick();
    }

    public void ClickedRestart()
    {
        GameController.instance.Restart();
        popupPause.SetActive(false);
        buttonSaveGame.interactable = true;

        //Sound
        SoundController.instance.PlayButtonClick();
    }

    public void ClickedReloadSave()
    {
        AdvertisementController.instance.ShowReloadSaveRewardVideo();

        //Analytics
        AnalyticsController.instance.LogEvent("ads_btn_click_reloadsave");

        //Sound
        SoundController.instance.PlayButtonClick();
    }

    void On_RestartGameEvent()
    {
        popupGameOver.SetActive(false);
        if (!IsOnboardedSaveGame())
        {
            GameController.instance.PauseGame();
            onboardingSave.SetActive(true);
        }
    }

    void On_StartGameEvent()
    {
        if (IsOnboarded())
        {
            ShowGameUI();

            //Check if we need to onboard for the save game
            if (!IsOnboardedSaveGame())
            {
                onboardingSave.SetActive(true);
            }
            else
            {
                GameController.instance.onboarded = true;
            }
        }
        else
        {
            onboardingMove.SetActive(true);
        }
    }

    void ShowGameUI()
    {
        buttonSettings.SetActive(true);
        buttonPause.SetActive(true);
        levelHolder.SetActive(true);
        highestLevelHolder.SetActive(true);
        comboUI.SetActive(true);
        buttonShare.SetActive(true);
        leaderboardButton.SetActive(true);
    }

    void On_GameOverEvent()
    {
        buttonReloadSave.SetActive(SaveGameController.instance.HasPreviousSavedGame());
        if (SaveGameController.instance.HasPreviousSavedGame())
        {
            AnalyticsController.instance.LogEvent("ads_btn_show_reloadsave");
        }

        popupGameOver.SetActive(true);
    }

    bool IsOnboarded()
    {
        if (PlayerPrefs.HasKey(PlayerPrefKeys.ONBOARDING))
        {
            return PlayerPrefs.GetInt(PlayerPrefKeys.ONBOARDING) == 1;
        }
        else
        {
            return false;
        }
    }

    void SetOnboarded(bool onboarded = true)
    {
        PlayerPrefs.SetInt(PlayerPrefKeys.ONBOARDING, onboarded ? 1 : 0);
    }

    bool IsOnboardedSaveGame()
    {
        if (PlayerPrefs.HasKey(PlayerPrefKeys.ONBOARDING_SAVE))
        {
            return PlayerPrefs.GetInt(PlayerPrefKeys.ONBOARDING_SAVE) == 1;
        }
        else
        {
            return false;
        }
    }

    void SetOnboardedSaveGame(bool onboarded = true)
    {
        PlayerPrefs.SetInt(PlayerPrefKeys.ONBOARDING_SAVE, onboarded ? 1 : 0);
    }

    private void OnDestroy()
    {
        GameController.instance.GameOverEvent -= On_GameOverEvent;
        GameController.instance.StartGameEvent -= On_StartGameEvent;
        GameController.instance.RestartGameEvent -= On_RestartGameEvent;
    }
}
