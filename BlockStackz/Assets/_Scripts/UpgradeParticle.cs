﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class UpgradeParticle : MonoBehaviour
{
    ParticleSystem[] _ps;

    private void Awake()
    {
        _ps = GetComponentsInChildren<ParticleSystem>();
    }

    public void SetColorAndPlay(Color color)
    {
        foreach(ParticleSystem ps in _ps)
        {
            MainModule main = ps.main;
            main.startColor = color;
        }

        GetComponent<ParticleSystem>().Play();

        Destroy(gameObject, 1);
    }
}
