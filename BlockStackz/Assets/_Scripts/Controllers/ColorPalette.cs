﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorPalette : MonoBehaviour
{
    public static ColorPalette instance;

    public bool showColors;
    public GameObject colorImage;
    public Color color1, color2, color3, color4;

    List<Color> colors = new List<Color>();

    private void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        CreatePaletteForColors(8, color1, color2);
        CreatePaletteForColors(8, color2, color3);
        CreatePaletteForColors(8, color3, color4);
    }

    void CreatePaletteForColors(int amount, Color col1, Color col2)
    {
        int steps = amount;
        float rdelta = (col2.r - col1.r) / steps;
        float gdelta = (col2.g - col1.g) / steps;
        float bdelta = (col2.b - col1.b) / steps;

        Color color = col1;
        for (int i = 0; i < steps - 1; i++)
        {
            color.r += rdelta;
            color.g += gdelta;
            color.b += bdelta;

            colors.Add(new Color(color.r, color.g, color.b));

            if (showColors)
            {
                GameObject obj = Instantiate(colorImage, transform);
                obj.GetComponent<Image>().color = new Color(color.r, color.g, color.b);
            }
        }
    }

    public Color GetColorForLevel(int level)
    {
        if (colors.Count > level - 1) {
            return colors[level - 1];
        }
        else
        {
            return colors[colors.Count - 1];
        }
    }
}
