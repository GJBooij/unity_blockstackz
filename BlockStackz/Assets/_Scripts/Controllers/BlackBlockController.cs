﻿using Pixelplacement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlackBlockController : MonoBehaviour
{
    public RectTransform container;
    public RectTransform steps;
    public RectTransform indicator;

    float containerHeight, nrOfSteps, stepAmount;
    int currentStep = 0;
    bool handleUpgrade = false, handleShot = true;

    // Start is called before the first frame update
    void Start()
    {
        GameController.instance.BlockShotEvent += On_BlockShot;
        GameController.instance.UpgradeEvent += On_Upgrade;
        GameController.instance.RestartGameEvent += On_GameRestart;

        containerHeight = container.sizeDelta.y;
        nrOfSteps = steps.childCount - 1;
        stepAmount = containerHeight / nrOfSteps;
    }
    
    void On_GameRestart()
    {
        currentStep = 0;
        indicator.anchoredPosition = new Vector2(indicator.anchoredPosition.x, 0);
        CancelInvoke("SpawnBlackBlock");
    }

    void On_BlockShot()
    {
        if (!handleShot) return;
        Tween.Finish(indicator.GetInstanceID());

        currentStep++;

        if(currentStep > nrOfSteps)
        {
            currentStep = (int)nrOfSteps;
            return;
        }

        Tween.AnchoredPosition(indicator, new Vector2(indicator.anchoredPosition.x, currentStep * stepAmount), 0.5f, 0f, Tween.EaseLinear);
        if (currentStep == nrOfSteps)
        {
            Invoke("SpawnBlackBlock", 0.5f);
        }

        handleUpgrade = true;
    }

    void On_Upgrade(int level)
    {
        if (!handleUpgrade) return;

        Tween.Stop(indicator.GetInstanceID());
        
        currentStep--;
        if (currentStep < 0) currentStep = 0;

        Tween.AnchoredPosition(indicator, new Vector2(indicator.anchoredPosition.x, currentStep * stepAmount), 0.5f, 0f, Tween.EaseLinear);
    }

    void SpawnBlackBlock()
    {
        if (currentStep != nrOfSteps) return;

        handleUpgrade = false;
        handleShot = false;

        Tween.LocalScale(indicator.transform, Vector3.one * 1.2f, 0.5f, 0, Tween.EaseSpring, Tween.LoopType.None, null,
            () =>
            {
                GameController.instance.CreateBlackBlock();
                Tween.AnchoredPosition(indicator, new Vector2(indicator.anchoredPosition.x, 0), 0.5f, 0, Tween.EaseInBack, Tween.LoopType.None, null, 
                    () =>
                    {
                        currentStep = 0;
                        handleShot = true;
                    }
                );
            }
        );
    }

    private void OnDestroy()
    {
        GameController.instance.BlockShotEvent -= On_BlockShot;
        GameController.instance.UpgradeEvent -= On_Upgrade;
        GameController.instance.RestartGameEvent -= On_GameRestart;
    }
}
