﻿using Pixelplacement;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
    //Singleton
    public static ScoreController instance;

    //Public variables
    [Header("Score")]
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI highScoreText;

    [Header("Levels")]
    public Image currentLevelBg;
    public TextMeshProUGUI currentLevelText;
    public Image highestLevelBg;
    public TextMeshProUGUI highestLevelText;

    //Private variables
    long score, highscore;
    int currentLevel, highestLevel;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        GameController.instance.UpgradeEvent += On_Upgrade;
        GameController.instance.GameOverEvent += On_GameOverEvent;
        GameController.instance.RestartGameEvent += On_RestartGameEvent;
        SettingsController.instance.BlockTextChanged += On_BlockTextChanged;

        highscore = GetHighscore();
        highScoreText.text = highscore.ToString();
        scoreText.text = score.ToString();

        highestLevel = GetHighestLevel();
        UpdateHighestLevelTextAndColors();

        currentLevel = 2;
        UpdateTextsAndColors();
    }

    public void LoadFromSavedGame(int lvl, long score)
    {
        currentLevel = lvl;
        this.score = score;
        scoreText.text = score.ToString();
        UpdateTextsAndColors();
    }

    //
    //SCORE
    //

    public void AddScore(long score)
    {
        this.score += score;
        scoreText.text = this.score.ToString();
    }

    void SaveHighScore(long score)
    {
        PlayerPrefs.SetString(PlayerPrefKeys.HIGHSCORE, score.ToString());
    }

    public long GetHighscore()
    {
        if (PlayerPrefs.HasKey(PlayerPrefKeys.HIGHSCORE))
        {
            return long.Parse(PlayerPrefs.GetString(PlayerPrefKeys.HIGHSCORE));
        }
        else
        {
            return 0;
        }
    }

    public long GetCurrentScore()
    {
        return score;
    }

    //
    //LEVEL
    //

    void On_Upgrade(int level)
    {
        if (currentLevel < level)
        {
            currentLevel = level;
            AnalyticsController.instance.LogEvent("score_level", "level", currentLevel);

            UpdateTextsAndColors();
        }

    }

    void SaveHighestLevel(int level)
    {
        PlayerPrefs.SetInt(PlayerPrefKeys.HIGHESTLEVEL, level);
    }

    public int GetHighestLevel()
    {
        if (PlayerPrefs.HasKey(PlayerPrefKeys.HIGHESTLEVEL))
        {
            return PlayerPrefs.GetInt(PlayerPrefKeys.HIGHESTLEVEL);
        }
        else
        {
            return 2;
        }
    }

    public int GetCurrentLevel()
    {
        return currentLevel;
    }

    void UpdateTextsAndColors()
    {
        Tween.LocalScale(currentLevelBg.transform, Vector3.one * 1.25f, 0.6f, 0, Tween.EaseWobble, Tween.LoopType.None,
            null,
            () => currentLevelBg.transform.localScale = Vector3.one
        );

        //Update Colors
        currentLevelBg.color = ColorPalette.instance.GetColorForLevel(currentLevel);

        //Update Texts
        currentLevelText.text = Utils.GetTextBySettingAndLevel(currentLevel);
    }

    void UpdateHighestLevelTextAndColors()
    {
        Tween.LocalScale(highestLevelBg.transform, Vector3.one * 1.25f, 0.6f, 0, Tween.EaseWobble, Tween.LoopType.None,
            null,
            () => highestLevelBg.transform.localScale = Vector3.one
        );

        highestLevelBg.color = ColorPalette.instance.GetColorForLevel(highestLevel);
        highestLevelText.text = Utils.GetTextBySettingAndLevel(highestLevel);
    }

    //
    //DEFAULT EVENTS
    //

    void On_BlockTextChanged(BlockText blockText)
    {
        UpdateTextsAndColors();
        UpdateHighestLevelTextAndColors();
    }

    void On_RestartGameEvent()
    {
        //Check if we need to save the the scores/levels
        On_GameOverEvent();

        //reset score
        score = 0;
        scoreText.text = score.ToString();

        //Reset level
        currentLevel = 2;
        UpdateTextsAndColors();
        UpdateHighestLevelTextAndColors();
    }

    void On_GameOverEvent()
    {
        if (score > highscore)
        {
            highscore = score;
            highScoreText.text = highscore.ToString();
            SaveHighScore(highscore);

            AnalyticsController.instance.LogEvent("score_new_highscore", "highscore", highscore);
        }

        if (currentLevel > highestLevel)
        {
            highestLevel = currentLevel;
            UpdateHighestLevelTextAndColors();
            SaveHighestLevel(highestLevel);

            AnalyticsController.instance.LogEvent("score_new_highlevel", "highlevel", highestLevel);
        }

        //Update the leaderboard
        LeaderboardController.instance.AddScoreToLeaderboard((int)Mathf.Pow(2, highestLevel), highscore);

        //Log Analytics
        AnalyticsController.instance.LogEvent("score_gameover", new Firebase.Analytics.Parameter[] {
                new Firebase.Analytics.Parameter(
                    "level", currentLevel),
                new Firebase.Analytics.Parameter(
                    "score", score)
            }
        );
    }

    private void OnDestroy()
    {
        GameController.instance.GameOverEvent -= On_GameOverEvent;
        GameController.instance.RestartGameEvent -= On_RestartGameEvent;
        GameController.instance.UpgradeEvent -= On_Upgrade;
        SettingsController.instance.BlockTextChanged -= On_BlockTextChanged;
    }
}
