﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using Pixelplacement;
using System.Collections;

public class AdvertisementController : MonoBehaviour, IUnityAdsListener
{
    public static AdvertisementController instance;

    public float timeBeforeAdSeconds = 180;
    public GameObject watchAdOnboarding;
    public GameObject watchAd;
    public Button watchAdButton;

    //Private variables
    private AudioSource _audioSource;
    private bool testMode = true;
#if UNITY_IOS
    private string gameId = "3833912";  //iOS Game ID, Unity Dashboard
#elif UNITY_ANDROID
    private string gameId = "3833913";  //Android Game ID, Unity Dashboard
#endif

    private string rewardAdPlacementId = "rewardvideo_block";
    private string reloadSaveAdPlacementId = "rewardvideo_reloadsave";
    private string bannerBottomAdPlacementId = "banner_bottom";
    private bool showButtonWhenAdReady;
    private int upgradesDone = 0;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        _audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {
        GameController.instance.UpgradeEvent += On_Upgrade;
        GameController.instance.StartGameEvent += On_GameStart;
        GameController.instance.GameOverEvent += On_GameOver;
        GameController.instance.RestartGameEvent += On_RestartGame;

        watchAd.SetActive(false);
        watchAdButton.onClick.AddListener(ShowRewardedVideo);
        _audioSource.clip = SoundController.instance.GetAdAvailable();

        // Initialize the Ads listener and service:
        Advertisement.AddListener(this);
        Advertisement.Initialize(gameId, testMode);

        //Initialize banner when ready
        StartCoroutine(ShowBannerWhenReady());
    }

    public void CloseWatchAdOnboarding()
    {
        watchAdOnboarding.SetActive(false);
    }

    public void LetsWatchOnboarding()
    {
        watchAdOnboarding.SetActive(false);
        PlayerPrefs.SetInt(PlayerPrefKeys.ONBOARDING_WATCHAD, 1);
        ShowRewardedVideo();
    }

    public void ShowReloadSaveRewardVideo()
    {
        if (Advertisement.IsReady(reloadSaveAdPlacementId))
        {
            Advertisement.Show(reloadSaveAdPlacementId);
            AnalyticsController.instance.LogEvent("ads_show", "placement", reloadSaveAdPlacementId);
        }
    }

    /// 
    /// Private Functions
    /// 

    void On_Upgrade(int level)
    {
        upgradesDone++;
    }

    void On_GameStart()
    {
        Invoke("ShowWatchAdButton", timeBeforeAdSeconds);
    }

    void On_GameOver()
    {
        CancelInvoke("ShowWatchAdButton");
        watchAd.SetActive(false);
        upgradesDone = 0;
    }

    void On_RestartGame()
    {
        Invoke("ShowWatchAdButton", timeBeforeAdSeconds);
        watchAd.SetActive(false);
        upgradesDone = 0;
    }

    IEnumerator ShowBannerWhenReady()
    {
        while (!Advertisement.isInitialized)
        {
            yield return new WaitForSeconds(0.5f);
        }

        //Load the banner ad
        BannerLoadOptions loadOptions = new BannerLoadOptions
        {
            loadCallback = OnBannerLoaded,
            errorCallback = OnBannerError
        };
        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
        Advertisement.Banner.Load(bannerBottomAdPlacementId, loadOptions);
    }

    void OnBannerLoaded()
    {
        Advertisement.Banner.SetPosition(BannerPosition.BOTTOM_CENTER);
        Advertisement.Banner.Show(bannerBottomAdPlacementId);
        AnalyticsController.instance.LogEvent("ads_show", "placement", bannerBottomAdPlacementId);
    }
    void OnBannerError(string error)
    {
        AnalyticsController.instance.LogEvent("ads_failed", "placement", bannerBottomAdPlacementId);
    }

    void ShowWatchAdButton()
    {
        if (Advertisement.IsReady(rewardAdPlacementId))
        {
            if (upgradesDone < 250)
            {
                Invoke("ShowWatchAdButton", 10);
                return;
            }

            AnalyticsController.instance.LogEvent("ads_btn_show_rewardblock");

            watchAd.SetActive(true);
            showButtonWhenAdReady = false;

            Tween.LocalScale(watchAd.transform.GetChild(0), Vector3.one * 1.4f, 0.6f, 0, Tween.EaseSpring, Tween.LoopType.None,
                null,
                () => watchAd.transform.GetChild(0).localScale = Vector3.one
            );
            _audioSource.Play();
        }
        else
        {
            showButtonWhenAdReady = true;
        }
    }

    void ShowRewardedVideo()
    {
        if (!IsOnboarded())
        {
            watchAdOnboarding.SetActive(true);
            return;
        }

        Advertisement.Show(rewardAdPlacementId);
        AnalyticsController.instance.LogEvent("ads_btn_click_rewardblock");
        AnalyticsController.instance.LogEvent("ads_show", "placement", rewardAdPlacementId);
    }

    bool IsOnboarded()
    {
        if (PlayerPrefs.HasKey(PlayerPrefKeys.ONBOARDING_WATCHAD))
        {
            return PlayerPrefs.GetInt(PlayerPrefKeys.ONBOARDING_WATCHAD) == 1;
        }
        else
        {
            return false;
        }
    }

/// 
/// Implement IUnityAdsListener interface methods:
/// 

    public void OnUnityAdsReady(string placementId)
    {
        // If the ready Placement is rewarded, activate the button: 
        if (placementId == rewardAdPlacementId)
        {
            if (showButtonWhenAdReady)
            {
                ShowWatchAdButton();
            }
        }
    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished)
        {
            if(placementId == rewardAdPlacementId)
            {
                // Reward the user for watching the ad to completion.
                GameController.instance.CreateVideoAwardedBlock();
            }
            else if(placementId == reloadSaveAdPlacementId)
            {
                GameController.instance.ReloadLastSave();
            }

            AnalyticsController.instance.LogEvent("ads_finished", "placement", placementId);
        }
        else if (showResult == ShowResult.Skipped)
        {
            // Do not reward the user for skipping the ad.
            AnalyticsController.instance.LogEvent("ads_skipped", "placement", placementId);
        }
        else if (showResult == ShowResult.Failed)
        {
            Debug.LogWarning("The ad did not finish due to an error: " + placementId);
            AnalyticsController.instance.LogEvent("ads_failed", "placement", placementId);
        }

        if (placementId == rewardAdPlacementId)
        {
            upgradesDone = 0;
            watchAd.SetActive(false);
            Invoke("ShowWatchAdButton", timeBeforeAdSeconds);
        }
    }

    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
        Debug.LogError("[AdvertisementController] Error: " + message);
    }

    public void OnUnityAdsDidStart(string placementId)
    {
        // Optional actions to take when the end-users triggers an ad.
    }

    private void OnDestroy()
    {
        GameController.instance.GameOverEvent -= On_GameOver;
        GameController.instance.RestartGameEvent -= On_RestartGame;
        watchAdButton.onClick.RemoveListener(ShowRewardedVideo);
        Advertisement.RemoveListener(this);
    }

}
