﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveGameController : MonoBehaviour
{
    public static SaveGameController instance;

    [Serializable]
    class BlockData
    {
        public int lvl;
        public bool isBlack;
        public Vector3 position;
    }

    [Serializable]
    class SaveData
    {
        public List<BlockData> blocks = new List<BlockData>();
        public int level;
        public long score;
    }

    public GameObject blocksContainer;
    public GameObject blockPrefab;
    public bool reloadSaveUsedThisGame = false;
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Start()
    {
        GameController.instance.GameOverEvent += DeleteSavedGame;
        GameController.instance.RestartGameEvent += DeleteSavedGame;
        if (!HasSavedGame())
        {
            DeletePreviousSavedGame();
        }
    }

    public void SaveGame()
    {
        SaveData savedata = new SaveData();

        //Get block data
        foreach (Block block in blocksContainer.GetComponentsInChildren<Block>())
        {
            if (block.shot)
            {
                BlockData newBlockData = new BlockData();
                newBlockData.lvl = block.GetCurrentLevel();
                newBlockData.isBlack = block.GetIsBlackBlock();
                newBlockData.position = block.transform.position;

                savedata.blocks.Add(newBlockData);
            }
        }

        //Get level and score data
        savedata.level = ScoreController.instance.GetCurrentLevel();
        savedata.score = ScoreController.instance.GetCurrentScore();

        string str = JsonUtility.ToJson(savedata);
        PlayerPrefs.SetString(PlayerPrefKeys.SAVEDGAME, str);
        if (!HasPreviousSavedGame() && !reloadSaveUsedThisGame)
        {
            PlayerPrefs.SetString(PlayerPrefKeys.PREVIOUSSAVEDGAME, str);
        }

        Utils.ShowAndroidToastMessage("Game Saved!");
        Debug.Log("Game Saved! level: " + savedata.level);
    }

    public void LoadSavedGame(Action cb_loadDone = null, bool loadPrevSave = false)
    {
        SaveData saveData = JsonUtility.FromJson<SaveData>(PlayerPrefs.GetString(loadPrevSave ? PlayerPrefKeys.PREVIOUSSAVEDGAME : PlayerPrefKeys.SAVEDGAME));
        if (loadPrevSave)
        {
            reloadSaveUsedThisGame = true;
            DeletePreviousSavedGame();
        }

        //Make sure the blocksContainer is completely empty
        foreach (Transform child in blocksContainer.transform)
        {
            Destroy(child.gameObject);
        }

        //Create blocks for all that are saved
        foreach (BlockData blockData in saveData.blocks)
        {
            GameObject newBlock = Instantiate(blockPrefab, blockData.position, Quaternion.identity, blocksContainer.transform);
            Block block = newBlock.GetComponent<Block>();
            block.SpawnFromSaveGame(blockData.lvl, blockData.isBlack);
        }

        //Set the correct score and level
        ScoreController.instance.LoadFromSavedGame(saveData.level, saveData.score);

        //Call the callback function after loading is complete
        cb_loadDone?.Invoke();
    }

    public void DeleteSavedGame()
    {
        if (PlayerPrefs.HasKey(PlayerPrefKeys.SAVEDGAME) && !reloadSaveUsedThisGame)
        {
            PlayerPrefs.SetString(PlayerPrefKeys.PREVIOUSSAVEDGAME, PlayerPrefs.GetString(PlayerPrefKeys.SAVEDGAME));
        }
        PlayerPrefs.DeleteKey(PlayerPrefKeys.SAVEDGAME);
    }

    public void DeletePreviousSavedGame()
    {
        PlayerPrefs.DeleteKey(PlayerPrefKeys.PREVIOUSSAVEDGAME);
    }

    public bool HasSavedGame()
    {
        return PlayerPrefs.HasKey(PlayerPrefKeys.SAVEDGAME);
    }

    public bool HasPreviousSavedGame()
    {
        return PlayerPrefs.HasKey(PlayerPrefKeys.PREVIOUSSAVEDGAME);
    }

    private void OnDestroy()
    {
        GameController.instance.GameOverEvent -= DeleteSavedGame;
        GameController.instance.RestartGameEvent -= DeleteSavedGame;
    }
}
