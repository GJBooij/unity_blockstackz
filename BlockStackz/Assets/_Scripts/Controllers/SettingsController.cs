﻿using System;
using UnityEngine;

[Serializable]
public enum BlockText
{
    POW2,
    Number,
    RomanNumber
}

[Serializable]
public class Background
{
    public string name;
    public bool isColor;
    public Color color;
}

[Serializable]
public class Settings
{
    public Background background = new Background();
    public BlockText blockText = BlockText.POW2;
}

public class SettingsController : MonoBehaviour
{
    public static SettingsController instance;

    public Camera _camera;
    public Background defaultBackground;
    public BlockText defaultBlockText;

    Settings settings;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        LoadSettingsData();
    }

    private void Start()
    {
        if (settings.background.isColor)
        {
            _camera.backgroundColor = settings.background.color;
        }
    }

    /// <summary>
    /// Called when the game starts
    /// </summary>
    public delegate void _BackgroundChanged(Background newBG);
    public event _BackgroundChanged BackgroundChanged;

    /// <summary>
    /// Called when the game starts
    /// </summary>
    public delegate void _BlockTextChanged(BlockText blockText);
    public event _BlockTextChanged BlockTextChanged;

    public void SetBackground(Background newBG, bool save = true)
    {
        settings.background = newBG;
        if (save) SaveSettingsData();

        //For now this logic is here, in the future might need to be moved.
        if (newBG.isColor)
        {
            _camera.backgroundColor = newBG.color;
        }

        BackgroundChanged?.Invoke(newBG);
    }

    public Background GetBackground()
    {
        return settings.background;
    }

    public void SetBlockText(BlockText blockText, bool save = true)
    {
        settings.blockText = blockText;
        if (save) SaveSettingsData();

        BlockTextChanged?.Invoke(blockText);
    }

    public BlockText GetBlockText()
    {
        return settings.blockText;
    }

    /// <summary>
    /// Loads the settings data from PlayerPrefs
    /// </summary>
    void LoadSettingsData()
    {
        if (PlayerPrefs.HasKey(PlayerPrefKeys.SETTINGS))
        {
            settings = JsonUtility.FromJson<Settings>(PlayerPrefs.GetString(PlayerPrefKeys.SETTINGS));
        }
        else
        {
            //Create new settings data with default settings
            settings = new Settings();
            settings.background = defaultBackground;
            settings.blockText = defaultBlockText;

            SaveSettingsData();
        }
    }

    /// <summary>
    /// Saves the settings data to PlayerPrefs
    /// </summary>
    public void SaveSettingsData()
    {
        string jsonString = JsonUtility.ToJson(settings);
        PlayerPrefs.SetString(PlayerPrefKeys.SETTINGS, jsonString);
    }

    /// <summary>
    /// Deletes the settings data from PlayerPrefs
    /// </summary>
    public void DeletePlayerProfile()
    {
        PlayerPrefs.DeleteKey(PlayerPrefKeys.SETTINGS);
    }
}
