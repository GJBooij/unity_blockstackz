﻿using Pixelplacement;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ComboController : MonoBehaviour
{
    public static ComboController instance;

    public RectTransform container;
    public RectTransform indicator;
    public RectTransform dangerIndicator;
    //public TextMeshProUGUI comboLevelText;

    public int comboLevel = 1;

    float stepAmount;
    int currentStep = 0;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        GameController.instance.UpgradeEvent += On_Upgrade;
        GameController.instance.BlockShotEvent += On_BlockShot;
        GameController.instance.RestartGameEvent += On_GameRestart;

        //stepAmount = 7,5
        stepAmount = container.GetComponent<RectTransform>().sizeDelta.y / 32;    
        currentStep = 0;
        indicator.sizeDelta = new Vector2(indicator.sizeDelta.x, 0);
    }

    void On_BlockShot()
    {
        currentStep = Mathf.Clamp(currentStep - 1, 0, 32);
        float indicatorSize = Mathf.Clamp(currentStep * stepAmount, 0, container.GetComponent<RectTransform>().sizeDelta.y);

        Tween.Finish(indicator.GetInstanceID());
        Tween.Size(indicator, new Vector2(indicator.sizeDelta.x, indicatorSize), 0.5f, 0);
    }

    void On_Upgrade(int level)
    {
        currentStep = Mathf.Clamp(currentStep + 2, 0, 32);
        float indicatorSize = Mathf.Clamp(currentStep * stepAmount, 0, container.GetComponent<RectTransform>().sizeDelta.y);

        if(currentStep == 32)
        {
            GameController.instance.CreateBlackBlock();
            currentStep = 0;
            indicatorSize = Mathf.Clamp(currentStep * stepAmount, 0, container.GetComponent<RectTransform>().sizeDelta.y);

            Tween.LocalScale(dangerIndicator.transform, Vector3.one * 2, 0.6f, 0, Tween.EaseSpring, Tween.LoopType.None,
                null,
                () => dangerIndicator.transform.localScale = Vector3.one
            );
        }

        Tween.Finish(indicator.GetInstanceID());
        Tween.Size(indicator, new Vector2(indicator.sizeDelta.x, indicatorSize), 0.5f, 0);
    }

    void On_GameRestart()
    {
        currentStep = 0;
        Tween.Stop(indicator.GetInstanceID());
        indicator.sizeDelta = new Vector2(indicator.sizeDelta.x, 0);
    }

    private void OnDestroy()
    {
        GameController.instance.UpgradeEvent -= On_Upgrade;
        GameController.instance.BlockShotEvent -= On_BlockShot;
        GameController.instance.RestartGameEvent -= On_GameRestart;
    }
}
