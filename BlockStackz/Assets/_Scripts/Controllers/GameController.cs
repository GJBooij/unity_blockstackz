﻿using Pixelplacement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameController : MonoBehaviour
{
    public static GameController instance;

    //public variables
    public LayerMask layerMask;
    public GameObject blockPrefab;
    public GameObject blocksContainer;
    public GameObject pusherPrefab;
    public Transform spawnPoint;
    public Transform spawnPointBlack;
    public Transform pusherSpawnPoint;
    public Transform line;
    public GameObject backCollider;
    public float force = 24;

    //public hidden variables
    [HideInInspector]
    public bool onboarded;
    [HideInInspector]
    public bool gameOver;

    /// <summary>
    /// Called when the game starts
    /// </summary>
    public delegate void _StartGame();
    public event _StartGame StartGameEvent;

    /// <summary>
    /// Called when the game restarts
    /// </summary>
    public delegate void _RestartGame();
    public event _RestartGame RestartGameEvent;

    /// <summary>
    /// Called when the game is over
    /// </summary>
    public delegate void _GameOver();
    public event _GameOver GameOverEvent;

    /// <summary>
    /// Called when the current block is shot
    /// </summary>
    public delegate void _BlockShot();
    public event _BlockShot BlockShotEvent;

    /// <summary>
    /// Called when a block upgraded
    /// </summary>
    public delegate void _BlockUpgrade(int level);
    public event _BlockUpgrade UpgradeEvent;

    //Private variables
    bool started, shot, moving, pusherActive, canShoot;
    bool backPressed, saveGameLoaded;
    GameObject currentBlock;
    Camera _camera;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        Application.targetFrameRate = 30;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Screen.fullScreen = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        _camera = Camera.main;

        if (SaveGameController.instance.HasSavedGame())
        {
            saveGameLoaded = false;
            SaveGameController.instance.LoadSavedGame(SaveGameLoaded);
        }
        else
        {
            saveGameLoaded = true;
        }
    }

    void SaveGameLoaded()
    {
        saveGameLoaded = true;
    }

    public void StartGame()
    {
        canShoot = true;
        CreateNewBlock();
        StartGameEvent?.Invoke();

        //Sound
        SoundController.instance.PlayButtonClick();

        StartCoroutine(WaitForStart());
    }

    public void PauseGame()
    {
        canShoot = false;

        //Sound
        SoundController.instance.PlayButtonClick();

        //Analytics
        AnalyticsController.instance.LogEvent("game_pause");
    }

    public void ContinueGame()
    {
        canShoot = true;

        AnalyticsController.instance.LogEvent("game_continue");
    }

    IEnumerator WaitForStart()
    {
        yield return new WaitForEndOfFrame();
        if(!saveGameLoaded) yield return new WaitUntil(() => saveGameLoaded = true);
        started = true;
    }

    void SetBackPressedFalse()
    {
        backPressed = false;
    }

    // Update is called once per frame
    void Update()
    {
#if UNITY_ANDROID
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!backPressed)
            {
                backPressed = true;
                Utils.ShowAndroidToastMessage("Press back again to quit");
                Invoke("SetBackPressedFalse", 3.0f);
            }
            else
            {
                Application.Quit();
            }
        }
#endif

        if (!started || gameOver || (onboarded && IsPointerOverUIElement())) return;

        if(!shot)
        {
            RaycastHit hit;

#if UNITY_EDITOR

            //if (Input.GetKeyDown(KeyCode.Space))
            //{
            //    CreateBlackBlock();
            //}

            Ray ray = _camera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 100, layerMask))
            {
                Transform objectHit = hit.transform;

                // Do something with the object that was hit by the raycast.
                if (objectHit.tag == "Ground")
                {
                    MoveCurrentBlock(hit);
                }
            }

            if (onboarded && moving && Input.GetMouseButtonDown(0))
            {
                ShootCurrentBlock();
                return;
            }
#else
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                if(touch.phase == TouchPhase.Began || touch.phase == TouchPhase.Moved)
                {
                    Ray ray = _camera.ScreenPointToRay(touch.position);

                    if (Physics.Raycast(ray, out hit, 100, layerMask))
                    {
                        Transform objectHit = hit.transform;

                        // Do something with the object that was hit by the raycast.
                        if (objectHit.tag == "Ground")
                        {
                            MoveCurrentBlock(hit);
                        }
                    }
                }
                
                if(onboarded && moving && touch.phase == TouchPhase.Ended)
                {
                    ShootCurrentBlock();
                    return;
                }
            }
#endif
        }
    }

    /// <summary>
    /// Function to move the current block that the user is controlling.
    /// </summary>
    /// <param name="hit"></param>
    void MoveCurrentBlock(RaycastHit hit)
    {
        moving = true; 

        Vector3 newBlockPos = new Vector3(Mathf.Clamp(hit.point.x, -3.25f, 3.25f), Mathf.Clamp(hit.point.y + 1, 0.5f, hit.point.y + 1f), Mathf.Clamp(hit.point.z, -3f, -0.6f));
        currentBlock.transform.position = newBlockPos;

        float extraForce = (Mathf.Abs(line.position.z - currentBlock.transform.position.z) - 1.25f) * 5;
        currentBlock.GetComponent<Block>().shakeAmount = 1 + (extraForce / 100);
        currentBlock.GetComponent<Block>().Shake();
    }

    /// <summary>
    /// Function to shoot the current block that the user is controlling.
    /// </summary>
    void ShootCurrentBlock()
    {
        if (!canShoot) return;

        //Calculate extra force
        float extraForce = (Mathf.Abs(line.position.z - currentBlock.transform.position.z) - 0.75f) * 8;

        //Move the current block forward
        currentBlock.GetComponent<Rigidbody>().AddForce(currentBlock.transform.forward * (force + extraForce), ForceMode.VelocityChange);
        currentBlock.GetComponent<Block>().IsShot();
        currentBlock = null;
        shot = true;
        moving = false;

        BlockShotEvent?.Invoke();

        //Play sound
        SoundController.instance.PlayShootBlock();

        //Create a new block after 0.3s
        Invoke("CreateNewBlock", 0.3f);
    }


    /// <summary>
    /// Function that creates a new block at the spawnpoint and adds it to the blocks container
    /// </summary>
    void CreateNewBlock()
    {
        currentBlock = Instantiate(blockPrefab, spawnPoint.position, Quaternion.identity, blocksContainer.transform);
        currentBlock.GetComponent<Rigidbody>().freezeRotation = true;
        currentBlock.GetComponent<Rigidbody>().useGravity = false;
        currentBlock.GetComponent<Block>().Spawn();
        shot = false;
    }

    /// <summary>
    /// Function that creates a new block at the spawnpoint and adds it to the blocks container
    /// </summary>
    public void CreateBlackBlock()
    {
        int[] rnd = new int[] { 4, 4, 5, 5, 5, 5, 6, 6 };

        GameObject blackBlock = Instantiate(blockPrefab, spawnPointBlack.position + new Vector3((int)Random.Range(-1, 2), 0, (int)Random.Range(-2, 3)), Quaternion.identity, blocksContainer.transform);
        blackBlock.GetComponent<Rigidbody>().freezeRotation = false;
        blackBlock.GetComponent<Rigidbody>().useGravity = true;
        blackBlock.GetComponent<Block>().SpawnAsBlackBlock(rnd[Random.Range(0, rnd.Length)]);
        blackBlock = null;
    }

    /// <summary>
    /// Function that creates a new block at the spawnpoint and adds it to the blocks container
    /// Used for video ad rewarding.
    /// </summary>
    public void CreateVideoAwardedBlock()
    {
        int blockLevel = Random.Range(ScoreController.instance.GetCurrentLevel() - 2, ScoreController.instance.GetCurrentLevel());
        if (blockLevel == 0) blockLevel++;

        GameObject blackBlock = Instantiate(blockPrefab, spawnPointBlack.position + new Vector3((int)Random.Range(-1, 2), 0, (int)Random.Range(-2, 3)), Quaternion.identity, blocksContainer.transform);
        blackBlock.GetComponent<Rigidbody>().freezeRotation = false;
        blackBlock.GetComponent<Rigidbody>().useGravity = true;
        blackBlock.GetComponent<Block>().SpawnAsBlackBlock(blockLevel);
        blackBlock = null;
    }

    public void CreatePusher()
    {
        if (pusherActive) return;

        GameObject pusher = Instantiate(pusherPrefab, pusherSpawnPoint.position, Quaternion.identity);
        pusherActive = true;
        canShoot = false;

        //Move the pusher
        Tween.Position(pusher.transform, pusher.transform.position + new Vector3(0, 4f, 0), 1f, 0, null, Tween.LoopType.None, null,
            () => Tween.LocalScale(pusher.transform, new Vector3(10, 1f, 0.5f), 0.5f, 0, Tween.EaseInOutStrong, Tween.LoopType.None, null,
                () => {
                    canShoot = true;
                    backCollider.transform.localPosition = new Vector3(0, 0, -10);
                    Tween.Position(pusher.transform, pusher.transform.position + new Vector3(0, 0, 18), 4f, 0, Tween.EaseOutStrong, Tween.LoopType.None, null,
                        () => Tween.LocalScale(pusher.transform, Vector3.zero, 0.6f, 0, Tween.EaseInOutStrong, Tween.LoopType.None, null,
                            () => DestroyPusher(pusher))
                    );
                })   
        );
    }

    void DestroyPusher(GameObject pusher)
    {
        Destroy(pusher);
        backCollider.transform.localPosition = Vector3.zero;
        pusherActive = false;
    }

    /// <summary>
    /// Function called by block's collision detection when it goes over the line
    /// Fires event: GameOverEvent
    /// </summary>
    public void GameOver()
    {
        gameOver = true;
        if (currentBlock)
        {
            currentBlock.GetComponent<Rigidbody>().freezeRotation = false;
            currentBlock.GetComponent<Rigidbody>().useGravity = true;
            currentBlock.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
        GameOverEvent?.Invoke();
    }

    /// <summary>
    /// Function to restart the game.
    /// Fires event: RestartGameEvent
    /// </summary>
    public void Restart()
    {
        started = false;

        foreach (Transform child in blocksContainer.transform)
        {
            Destroy(child.gameObject);
        }

        SaveGameController.instance.reloadSaveUsedThisGame = false;
        RestartGameEvent?.Invoke();
        gameOver = false;
        CreateNewBlock();
        canShoot = true;

        StartCoroutine(WaitForStart());

        //Sound
        SoundController.instance.PlayButtonClick();
    }

    public void ReloadLastSave()
    {
        RestartGameEvent?.Invoke();
        
        saveGameLoaded = false;
        StartCoroutine(WaitForStart());
        SaveGameController.instance.LoadSavedGame(() =>
        {
            gameOver = false;
            CreateNewBlock();
            canShoot = true;
            
            SaveGameLoaded();
        }, true);
    }

    public void Quit()
    {
        AnalyticsController.instance.LogEvent("game_quit");
        Application.Quit();
    }

    public void FireUpgradeEvent(int level)
    {
        UpgradeEvent?.Invoke(level);
    }

    //
    // Helper functions
    //

    ///Returns 'true' if we touched or hovering on Unity UI element.
    bool IsPointerOverUIElement()
    {
        return IsPointerOverUIElement(GetEventSystemRaycastResults());
    }

    ///Returns 'true' if we touched or hovering on Unity UI element.
    bool IsPointerOverUIElement(List<RaycastResult> eventSystemRaysastResults)
    {
        for (int index = 0; index < eventSystemRaysastResults.Count; index++)
        {
            RaycastResult curRaysastResult = eventSystemRaysastResults[index];
            if (curRaysastResult.gameObject.layer == LayerMask.NameToLayer("UI"))
                return true;
        }
        return false;
    }

    ///Gets all event systen raycast results of current mouse or touch position.
    List<RaycastResult> GetEventSystemRaycastResults()
    {
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.mousePosition;
        List<RaycastResult> raysastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, raysastResults);
        return raysastResults;
    }
}
