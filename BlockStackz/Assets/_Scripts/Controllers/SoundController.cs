﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundController : MonoBehaviour
{
    public static SoundController instance;

    public AudioClip buttonClick;
    public AudioClip shoot;
    public AudioClip upgrade;
    public AudioClip spawnBlack;
    public AudioClip adAvailable;

    //Private variables
    private AudioSource audioSource;
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        audioSource = GetComponent<AudioSource>();
        audioSource.loop = false;
    }

    public void PlayButtonClick()
    {
        PlaySound(buttonClick, 0.8f, 0.8f);
    }

    public void PlayShootBlock()
    {
        PlaySound(upgrade, 0.7f, 1.3f);
    }

    public void PlayUpgrade()
    {
        PlaySound(shoot, 1.0f, 1.6f);
    }

    public void PlaySpawnBlack()
    {
        PlaySound(spawnBlack);
    }

    public void PlaySound(AudioClip clip, float volume = 1, float pitch = 1)
    {
        if (audioSource.isPlaying) return;
        audioSource.pitch = pitch;
        audioSource.volume = volume;
        audioSource.clip = clip;
        audioSource.Play();
    }

    //Getters
    public AudioClip GetShootSound() { return shoot; }
    public AudioClip GetUpgradeSound() { return upgrade; }
    public AudioClip GetSpawnBlack() { return spawnBlack; }
    public AudioClip GetAdAvailable() { return adAvailable; }
}
