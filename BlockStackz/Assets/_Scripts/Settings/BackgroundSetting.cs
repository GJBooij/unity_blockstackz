﻿using DanielLochner.Assets.SimpleScrollSnap;
using UnityEngine;

public class BackgroundSetting : MonoBehaviour
{
    public SimpleScrollSnap scrollSnap;

    void OnEnable()
    {
        if (SettingsController.instance == null) return;

        //Find the panel that belongs to the current setting
        Background currentBG = SettingsController.instance.GetBackground();
        Debug.Log(currentBG.color);

        int index = 0;
        foreach(RectTransform child in scrollSnap.Content)
        {
            if (child.GetComponent<BackgroundSettingHelper>().newBackground.color == currentBG.color)
            {
                scrollSnap.startingPanel = index;
                scrollSnap.GoToPanel(index);
                break;
            }
            index++;
        }
    }

    public void OnPanelChanged()
    {
        SettingsController.instance.SetBackground(scrollSnap.Content.GetChild(scrollSnap.TargetPanel).GetComponent<BackgroundSettingHelper>().newBackground);
    }
}
