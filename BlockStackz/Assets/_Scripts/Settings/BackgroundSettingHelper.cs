﻿using UnityEngine;
using UnityEngine.UI;

public class BackgroundSettingHelper : MonoBehaviour
{
    public Background newBackground;

    private void Start()
    {
        if (newBackground.isColor && GetComponent<Image>() != null)
        {
            GetComponent<Image>().color = newBackground.color;
        }
    }
}
