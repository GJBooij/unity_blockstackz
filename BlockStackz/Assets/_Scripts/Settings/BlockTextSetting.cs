﻿using DanielLochner.Assets.SimpleScrollSnap;
using UnityEngine;

public class BlockTextSetting : MonoBehaviour
{
    public SimpleScrollSnap scrollSnap;

    void OnEnable()
    {
        if (SettingsController.instance == null) return;

        //Find the panel that belongs to the current setting
        BlockText currentBlockText = SettingsController.instance.GetBlockText();
        int index = 0;
        foreach (RectTransform child in scrollSnap.Content)
        {
            if (child.GetComponent<BlockTextSettingHelper>().newBlockText == currentBlockText)
            {
                scrollSnap.startingPanel = index;
                scrollSnap.GoToPanel(index);
                break;
            }
            index++;
        }
    }

    public void OnPanelChanged()
    {
        SettingsController.instance.SetBlockText(scrollSnap.Content.GetChild(scrollSnap.TargetPanel).GetComponent<BlockTextSettingHelper>().newBlockText);
    }
}
