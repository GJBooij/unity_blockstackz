﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerPrefKeys
{
    public static readonly string SAVEDGAME = "PLAYERPREFS_SAVEDGAME";
    public static readonly string PREVIOUSSAVEDGAME = "PLAYERPREFS_PREVIOUSSAVEDGAME";
    public static readonly string SETTINGS = "PLAYERPREFS_SETTINGS";
    public static readonly string HIGHSCORE = "PLAYERPREFS_HIGHSCORE";
    public static readonly string HIGHESTLEVEL = "PLAYERPREFS_HIGHESTLEVEL";
    public static readonly string ONBOARDING = "PLAYERPREFS_ONBOARDING";
    public static readonly string ONBOARDING_SAVE = "PLAYERPREFS_ONBOARDING_SAVE";
    public static readonly string ONBOARDING_WATCHAD = "PLAYERPREFS_ONBOARDING_WATCHAD";
}
