﻿using Pixelplacement;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Block : MonoBehaviour
{
    public MeshRenderer _renderer;
    public Rigidbody _rb;
    public AudioSource _audioSource;
    public bool shot = false;
    public GameObject upgradeParticle;

    [HideInInspector]
    public float shakeAmount = 1.01f;

    int currentLvl = 0;
    bool hasLeftStartArea, collided, shaking;

    bool isBlackBlock;
    int blockerTurns;

    TextMeshPro[] texts;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        _audioSource = GetComponent<AudioSource>();
        texts = GetComponentsInChildren<TextMeshPro>();
    }

    /// <summary>
    /// Spawn function to spawn the block, called from the GameController.
    /// </summary>
    public void Spawn()
    {
        SettingsController.instance.BlockTextChanged += OnBlockTextChanged;

        Upgrade(true);
    }

    /// <summary>
    /// Spawn function used to spawn a block as a black block, called from the GameController.
    /// </summary>
    /// <param name="lvl"></param>
    public void SpawnAsBlackBlock(int lvl)
    {
        SettingsController.instance.BlockTextChanged += OnBlockTextChanged;

        currentLvl = lvl;
        isBlackBlock = true;

        _renderer.material.color = Color.black;
        //_renderer.material.color = ColorPalette.instance.GetColorForLevel(currentLvl);
        foreach (TextMeshPro text in texts)
        {
            text.text = Utils.GetTextBySettingAndLevel(currentLvl);
        }

        _renderer.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
        Tween.LocalScale(_renderer.transform, Vector3.one, 0.6f, 0, Tween.EaseSpring, Tween.LoopType.None,
            null,
            () => _renderer.transform.localScale = Vector3.one
        );

        shot = true;

        //Sound
        _audioSource.clip = SoundController.instance.GetSpawnBlack();
        _audioSource.Play();
    }

    /// <summary>
    /// Spawn function used for loading blocks from a save game, called from the SaveGameController.
    /// </summary>
    /// <param name="lvl"></param>
    /// <param name="black"></param>
    public void SpawnFromSaveGame(int lvl, bool black)
    {
        SettingsController.instance.BlockTextChanged += OnBlockTextChanged;

        currentLvl = lvl;
        isBlackBlock = black;

        //Set the correct lvl text.
        foreach (TextMeshPro text in texts)
        {
            text.text = Utils.GetTextBySettingAndLevel(currentLvl);
        }

        //Set the correct color
        if (isBlackBlock)
        {
            _renderer.material.color = Color.black;
        }
        else
        {
            _renderer.material.color = ColorPalette.instance.GetColorForLevel(currentLvl);
            
        }

        _rb.freezeRotation = false;
        _rb.useGravity = true;

        shot = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        HandleCollision(collision);
    }

    private void OnCollisionStay(Collision collision)
    {
        HandleCollision(collision);
    }

    void HandleCollision(Collision collision)
    {
        //Check if we are colliding with a block, isn't in a colliding transition they are both shot
        if (collision.transform.tag == "Block" && !collided && shot && collision.gameObject.GetComponent<Block>().shot)
        {
            //Turn of freeze and start using gravity
            _rb.freezeRotation = false;
            _rb.useGravity = true;

            //Get reference to the other block. If that's a blocker, just do nothing
            Block otherBlock = collision.gameObject.GetComponent<Block>();

            //Check if they are the same level, if so: upgrade one and re-use or destroy the other one.
            if (currentLvl == otherBlock.currentLvl)
            {
                collided = true;
                otherBlock.collided = true;

                Upgrade();
                _rb.AddForce(new Vector3(Random.Range(-0.1f, 0.1f), 6f, Random.Range(-0.1f, 0.1f)), ForceMode.Impulse);
                Destroy(otherBlock.gameObject);
            }
        }
    }

    //
    //Functions to handle leaving the starting area
    //

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Finish" && hasLeftStartArea && !GameController.instance.gameOver)
        {
            GameController.instance.GameOver();
        }
        else if (other.tag == "Void")
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "Finish" && hasLeftStartArea && !GameController.instance.gameOver)
        {
            GameController.instance.GameOver();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Finish" && shot)
        {
            hasLeftStartArea = true;
        }
    }

    void LeftStartArea()
    {
        hasLeftStartArea = true;
    }

    /// <summary>
    /// Function called by GameController when the block is shot by the user.
    /// </summary>
    public void IsShot()
    {
        shot = true;
        StopShaking();
        Invoke("LeftStartArea", 1.0f);
    }

    /// <summary>
    /// Function to upgrade the block
    /// </summary>
    /// <param name="spawn"></param>
    void Upgrade(bool spawn = false)
    {
        //Increase the lvl of the block
        if (spawn)
        {
            currentLvl = Random.Range(1, 3);
        }
        else
        {
            currentLvl++;

            //Fire an upgrade event
            GameController.instance.FireUpgradeEvent(currentLvl);
        }

        //Get new color and text
        _renderer.material.color = ColorPalette.instance.GetColorForLevel(currentLvl);
        foreach(TextMeshPro text in texts)
        {
            text.text = Utils.GetTextBySettingAndLevel(currentLvl);
        }
        collided = false;

        //Do some fancy stuff
        if (!spawn)
        {
            _renderer.transform.localScale = new Vector3(0.6f, 0.6f, 0.6f);
            Tween.LocalScale(_renderer.transform, Vector3.one, 0.6f, 0, Tween.EaseSpring, Tween.LoopType.None, 
                null, 
                () => _renderer.transform.localScale = Vector3.one
            );

            //Spawn some fancy particles
            GameObject particle = Instantiate(upgradeParticle, transform.position + new Vector3(0, 1.5f, 0), Quaternion.identity);
            particle.GetComponent<UpgradeParticle>().SetColorAndPlay(ColorPalette.instance.GetColorForLevel(currentLvl));

            //Add the score
            ScoreController.instance.AddScore((long)Mathf.Pow(2, currentLvl));

            //Sound
            //_audioSource.clip = SoundController.instance.GetUpgradeSound();
            //_audioSource.Play();
            SoundController.instance.PlayUpgrade();
        }
    }

    /// <summary>
    /// Called when the user has shot a block
    /// </summary>
    void DecreaseBlockerCounter()
    {
        blockerTurns--;
        foreach (TextMeshPro text in texts)
        {
            text.text = blockerTurns.ToString();
        }

        if (blockerTurns == 0)
        {
            Tween.LocalScale(transform, Vector3.zero, 0.6f, 0, Tween.EaseInOutStrong, Tween.LoopType.None,
                null,
                () => Destroy(gameObject)
            );
        }

    }

    /// <summary>
    /// Called when the user changed the block text setting.
    /// </summary>
    /// <param name="newBlockText"></param>
    void OnBlockTextChanged(BlockText newBlockText)
    {
        foreach (TextMeshPro text in texts)
        {
            text.text = Utils.GetTextBySettingAndLevel(currentLvl);
        }
    }

    //
    //Shaking force animation
    //

    public void Shake()
    {
        if (!shaking)
        {
            shaking = true;
            Tween.LocalScale(_renderer.transform, new Vector3(shakeAmount, shakeAmount, shakeAmount), 0.4f, 0, Tween.EaseWobble, Tween.LoopType.None, null, () => shaking = false);
        }
    }

    public void StopShaking()
    {
        _renderer.transform.localScale = Vector3.one;
        Tween.Finish(_renderer.GetInstanceID());
    }

    //
    // GETTERS
    //

    public int GetCurrentLevel()
    {
        return currentLvl;
    }

    public bool GetIsBlackBlock()
    {
        return isBlackBlock;
    }

    /// <summary>
    /// OnDestroy is called by the system when the gaeobject holding this script has been destroyed.
    /// </summary>
    private void OnDestroy()
    {
        Tween.Stop(_renderer.GetInstanceID());
        SettingsController.instance.BlockTextChanged -= OnBlockTextChanged;
        GameController.instance.BlockShotEvent -= DecreaseBlockerCounter;
    }
}
