﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;

public class AnalyticsController : MonoBehaviour
{
    public static AnalyticsController instance;

#if UNITY_EDITOR
    public bool disabled = true;
#else
    public bool disabled = false;
#endif

    //Private variables
    FirebaseApp app;
    bool initialized;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (disabled) return;

        //Subscribe to Events
        GameController.instance.StartGameEvent += On_StartGame;
        GameController.instance.RestartGameEvent += On_RestartGame;
        GameController.instance.UpgradeEvent += On_BlockUpgrade;
        GameController.instance.BlockShotEvent += On_BlockShot;
        GameController.instance.GameOverEvent += On_GameOver;

        // Initialize Firebase
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                // Crashlytics will use the DefaultInstance, as well;
                // this ensures that Crashlytics is initialized.
                app = FirebaseApp.DefaultInstance;

                // For testing, enable Debug logging.
                FirebaseApp.LogLevel = LogLevel.Error;

                // Set a flag here for indicating that your project is ready to use Firebase.
                initialized = true;

                Debug.Log("Firebase initialized!");

                //Log the game opens
                Firebase.Analytics.FirebaseAnalytics.LogEvent(Firebase.Analytics.FirebaseAnalytics.EventAppOpen);
            }
            else
            {
                Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
    }

    public void LogEvent(string eventName)
    {
        if (!initialized || disabled) return;

        Firebase.Analytics.FirebaseAnalytics.LogEvent(eventName);
    }

    public void LogEvent(string eventName, string paramName, double paramValue)
    {
        if (!initialized || disabled) return;

        Firebase.Analytics.FirebaseAnalytics.LogEvent(eventName, paramName, paramValue);
    }

    public void LogEvent(string eventName, string paramName, int paramValue)
    {
        if (!initialized || disabled) return;

        Firebase.Analytics.FirebaseAnalytics.LogEvent(eventName, paramName, paramValue);
    }

    public void LogEvent(string eventName, string paramName, string paramValue)
    {
        if (!initialized || disabled) return;

        Firebase.Analytics.FirebaseAnalytics.LogEvent(eventName, paramName, paramValue);
    }

    public void LogEvent(string eventName, Firebase.Analytics.Parameter[] parameters)
    {
        if (!initialized || disabled) return;

        Firebase.Analytics.FirebaseAnalytics.LogEvent(eventName, parameters);
    }

    void On_StartGame()
    {
        if (!initialized || disabled) return;

        Firebase.Analytics.FirebaseAnalytics.LogEvent("game_start");
    }

    void On_RestartGame()
    {
        if (!initialized || disabled) return;

        Firebase.Analytics.FirebaseAnalytics.LogEvent("game_restart");
    }

    void On_BlockShot()
    {
        if (!initialized || disabled) return;

        Firebase.Analytics.FirebaseAnalytics.LogEvent("block_shot");
    }

    void On_BlockUpgrade(int level)
    {
        if (!initialized || disabled) return;

        Firebase.Analytics.FirebaseAnalytics.LogEvent("block_upgrade", "level", level);
    }

    void On_GameOver()
    {
        if (!initialized || disabled) return;

        Firebase.Analytics.FirebaseAnalytics.LogEvent("game_over");
    }

    private void OnApplicationQuit()
    {
        if (!initialized || disabled) return;
        Firebase.Analytics.FirebaseAnalytics.LogEvent("application", "type", "quit");
    }

    private void OnApplicationPause(bool pause)
    {
        if (!initialized || disabled) return;

        Firebase.Analytics.FirebaseAnalytics.LogEvent("application", "type", pause ? "pause" : "continue");
    }

    private void OnApplicationFocus(bool focus)
    {
        if (!initialized || disabled) return;

        Firebase.Analytics.FirebaseAnalytics.LogEvent("application", "type", focus ? "focus" : "unfocus");
    }

    private void OnDestroy()
    {
        //Unsubscribe to Events
        GameController.instance.StartGameEvent -= On_StartGame;
        GameController.instance.RestartGameEvent -= On_RestartGame;
        GameController.instance.UpgradeEvent -= On_BlockUpgrade;
        GameController.instance.BlockShotEvent -= On_BlockShot;
        GameController.instance.GameOverEvent -= On_GameOver;
    }
}
