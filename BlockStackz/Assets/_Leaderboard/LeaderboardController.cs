﻿#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
#elif UNITY_IOS
using UnityEngine.SocialPlatforms;
#endif
using UnityEngine;

public class LeaderboardController : MonoBehaviour
{
    public static LeaderboardController instance;

    bool initialized;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
#if UNITY_ANDROID
        // Create client configuration
        PlayGamesClientConfiguration config = new
            PlayGamesClientConfiguration.Builder()
            .Build();

        // Enable debugging output (recommended)
        PlayGamesPlatform.DebugLogEnabled = true;

        // Initialize and activate the platform
        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.Activate();

        // Try silent sign-in
        PlayGamesPlatform.Instance.Authenticate(SilentSignInCallback, true);
#else
        // Authenticate
        Social.localUser.Authenticate(SilentSignInCallback);
#endif
        initialized = true;

    }

    public void SignIn()
    {
#if UNITY_ANDROID
        if (!PlayGamesPlatform.Instance.localUser.authenticated)
        {
            AnalyticsController.instance.LogEvent("gpgs_sign_in");
            // Sign in with Play Game Services, showing the consent dialog
            // by setting the second parameter to isSilent=false.
            PlayGamesPlatform.Instance.Authenticate(SignInCallback, false);
        }
#else
        // Authenticate
        AnalyticsController.instance.LogEvent("ios_sign_in");
        Social.localUser.Authenticate(SignInCallback);
#endif
    }

    public void SignOut()
    {
#if UNITY_ANDROID
        AnalyticsController.instance.LogEvent("gpgs_sign_out");
        PlayGamesPlatform.Instance.SignOut();
#endif
    }

    public void ShowLeaderboard()
    {
        //Sound
        SoundController.instance.PlayButtonClick();

#if UNITY_ANDROID
        if (PlayGamesPlatform.Instance.localUser.authenticated)
        {
            AnalyticsController.instance.LogEvent("gpgs_leaderboard_open");
            PlayGamesPlatform.Instance.ShowLeaderboardUI();
        }
        else
        {
            SignIn();
        }
#else
        if (Social.localUser.authenticated)
        {
            AnalyticsController.instance.LogEvent("ios_leaderboard_open");
            Social.ShowLeaderboardUI();
        }
        else
        {
            SignIn();
        }
#endif
    }

    public void AddScoreToLeaderboard(int level, long score)
    {
#if UNITY_ANDROID
        // Submit leaderboard scores, if authenticated
        if (PlayGamesPlatform.Instance.localUser.authenticated)
        {
            //Submit the highest block
            PlayGamesPlatform.Instance.ReportScore(level,
                GPGSIds.leaderboard_highest_block,
                (bool success) =>
                {
                    Debug.Log("Leaderboard - Highest Block - update success: " + success);
                });

            //Submit the highscore
            PlayGamesPlatform.Instance.ReportScore(score,
                GPGSIds.leaderboard_highest_score,
                (bool success) =>
                {
                    Debug.Log("Leaderboard - Highscore - update success: " + success);
                });
        }
#else
        //Submit to highest block.
        Social.ReportScore(level, "blockstackz_highest_block", success => {
            Debug.Log("Leaderboard - Highest Block - update success: " + success);
        });

        //Submit to highest score.
        Social.ReportScore(score, "blockstackz_highest_score", success => {
            Debug.Log("Leaderboard - Highscore - update success: " + success);
        });
#endif
    }

    void SilentSignInCallback(bool success)
    {
        if (success)
        {
#if UNITY_ANDROID
            AnalyticsController.instance.LogEvent("gpgs_silent_sign_in_success");
#else
            AnalyticsController.instance.LogEvent("ios_authenticate_success");
#endif
            Debug.Log("Silent sign-in success");
        }
        else
        {
#if UNITY_ANDROID
            AnalyticsController.instance.LogEvent("gpgs_silent_sign_in_failure");
#else
            AnalyticsController.instance.LogEvent("ios_authenticate_failure");
#endif
            Debug.Log("Silent sign-in failed");
        }
    }

    void SignInCallback(bool success)
    {
        if (success)
        {
#if UNITY_ANDROID
            AnalyticsController.instance.LogEvent("gpgs_sign_in_success");
#else
            AnalyticsController.instance.LogEvent("ios_authenticate_success");
#endif
            Debug.Log("(GPGS) Signed in!");

            //Update the leaderboard after sign-in
            if (ScoreController.instance.GetHighestLevel() > 2)
            {
                AddScoreToLeaderboard((int)Mathf.Pow(2, ScoreController.instance.GetHighestLevel()), ScoreController.instance.GetHighscore());
            }

            ShowLeaderboard();
        }
        else
        {
#if UNITY_ANDROID
            AnalyticsController.instance.LogEvent("gpgs_sign_in_failure");
#else
            AnalyticsController.instance.LogEvent("ios_authenticate_failure");
#endif
            Debug.Log("Sign-in failed");
        }
    }
}
